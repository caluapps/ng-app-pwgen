import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	constructor(private title: Title) { }

	ngOnInit() {
		this.title.setTitle('Password Generator by jD');
	}

	length = 0
	includeLetters = false
	includeNumbers = false
	includeSymbols = false
	password = ''


	onChangeLength(length: string) {
		const parsedLength = +length

		if (!isNaN(parsedLength))
			this.length = parsedLength
	}

	onChangeUseLetters() {
		this.includeLetters = !this.includeLetters
	}

	onChangeUseNumbers() {
		this.includeNumbers =!this.includeNumbers
	}

	onChangeUseSymbols() {
		this.includeSymbols = !this.includeSymbols
	}

	isValid() {
		if (this.length && (this.includeLetters || this.includeNumbers || this.includeSymbols))
			return false
		return true
	}

	randomIndex(start: number, end: number) {
		return Math.floor(Math.random() * end) + start
	}

	genPW() {
		const letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
		const numbers = '0123456789'
		const symbols = '!$€&=?.*#-'

		let validChars = ''
		let generatedPw = ''
		let i, randomIndex

		let changedIndexOrder = []

		for (i=0; i<3; i+=1) {
			while (i == changedIndexOrder.length) {
				randomIndex = this.randomIndex(0, 3)

				if (!changedIndexOrder.includes(randomIndex))
					changedIndexOrder.push(randomIndex)

				else if (changedIndexOrder.length == 2) {
					const lastIndex = [0, 1, 2].filter(n => !changedIndexOrder.includes(n))[0]
					changedIndexOrder.push(lastIndex)
				}
			}
		}

		if (this.includeLetters)
			validChars += letters

		if (this.includeNumbers)
			validChars += numbers

		if (this.includeSymbols)
			validChars += symbols

		for (i=0; i<this.length; i+=1) {
			randomIndex = this.randomIndex(0, validChars.length)
			generatedPw += validChars[randomIndex]
		}

		this.password = generatedPw
	}
}
